<%-- 
    Document   : index
    Created on : 07.12.2021, 22:46:03
    Author     : Ivan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Парикмахерская</title>
    </head>
    <body>
        <header>
            <center><h1>Парикмахерская</h1></center>
            <a href="index.jsp"><img alt="Логотип библиотеки" id="top-image" src="images/prhm.png"/></a>
            <div id="user-panel">
                <a href=""><img alt="Иконка юзера" src="images/usr.png"/></a>
                <a href="registration">Регистрация</a>
            </div>
        </header>
        <div id="main">
        <div id="main">
            <aside class="leftAside">
                <h2>Услуги</h2>
                <ul>
                    <li><a href="#">Запись на прием</a></li>
                    
                </ul>
            </aside>
            <section>
                <p>Записи на прием</p>
            <section>
                <article>
                    <h1>Шевцов Лев Сергеевич</h1>
                    <div class="text-article">                        
                        8809*****58 13:18 Нужно подровнять кончики. 
                    </div>
                    <div class="fotter-article">
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Дата статьи: 20.12.2012</span>
                    </div>
                </article>
                <article>
                    <h1>Моисеева Анастасия Максимовна</h1>
                    <div class="text-article">
                        8947*****85, 12:56, нужно покрасить волосы. 
                    </div>
                    <div class="fotter-article">
                        <span class="read"><a href="#">Читать...</a></span>
                        <span class="date-article">Дата статьи: 20.12.2012</span>
                        
                    </div>
                </article>
                                <article>
                    <h1>Зайцев Егор Глебович</h1>
                    <div class="text-article">
                        8545*****54, 15:47, постричься.  
                    </div>
                    <div class="fotter-article">
                        <span class="read"><a href="#">Читать...</a></span>
                        <span class="date-article">Дата статьи: 20.12.2012</span>
                        
                    </div>
                </article>
            </section>
        </div>
        <footer>
            <div>
                <span>ООО "ХЧЗЧТ"</span>
                <span><a target="_blanc" href="#"></a></span>
            </div>
        </footer>
    </body>
</html>